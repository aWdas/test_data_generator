package xyz.hadl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

/**
 * Created by jan on 16.12.15.
 */
public class RowGenerator {

    public static int id;
    public static Random r = new Random();

    public static String generateValidLine(){
        String s = "";
        s += generateID(true) + "\t" + generateString() + "\t"  + generateDate() + "\t" + r.nextInt(50000);
        return s;
    }

    public static String generateInvalidLine(){
        String s = "";
        int x = r.nextInt(4);

        switch (x) {
            case 0: s= generateID(false) + "\t" + generateString() + "\t" + generateString() + "\t" + generateDate() + "\t" + generateDate() + "\t" + r.nextInt(50000) + "\t" + r.nextInt(50000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000);
                break;
            case 1: s= generateID(true) + "\t" + generateString() + "\t" + generateString() + "\t" + generateDate() + "\t" + generateDate() + "\t" + generateString() + "\t" + r.nextInt(50000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000);
                break;
            case 2: s= generateID(true) + "\t" + r.nextInt(40000) + "\t" + generateString() + "\t" + generateDate() + "\t" + generateDate() + "\t" + r.nextInt(50000) + "\t" + r.nextInt(50000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000);
                break;
            case 3: s= generateID(true) + "\t" + generateString() + "\t" + generateDate() + "\t" + generateDate() + "\t" + generateDate() + "\t" + r.nextInt(50000) + "\t" + r.nextInt(50000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000) + "\t" + r.nextInt(4000);
                break;
        }

        return s;
    }

    public static String generateString(){
        String from = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String s = "";

        int x = r.nextInt(18) +1;
        for(int i = 0; i < x; i++){
            s += from.charAt(r.nextInt(from.length()));
        }

        return s;
    }

    public static int generateID(boolean valid){
        if(!valid){
            int wrongId = r.nextInt(id);
            return wrongId;
        }
        id += 1;
        return id;
    }

    public static String generateDate(){
        LocalDate date = LocalDate.of(r.nextInt(50) + 1971, r.nextInt(12)+1, r.nextInt(28) +1);
        return date.format(DateTimeFormatter.BASIC_ISO_DATE);
    }
    public static String generateGender() {
        if(r.nextInt(1)==1) {
            return "m";
        } else {
            return "f";
        }
    }
}
