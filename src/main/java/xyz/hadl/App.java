package xyz.hadl;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

/**
 * Hello world!
 *
 */

/*
CREATE TABLE IF NOT EXISTS `gamiphey`.`task` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY_KEY,
  `title` VARCHAR(60) NOT NULL,
  `description` TEXT NULL,
  `creationdate` DATE NULL,
  `deadline` DATE NULL,
  `delay_allowed` TINYINT(1) NULL,
  `min_score` INT NOT NULL,
  `max_bonusscore` INT NOT NULL,
  `solutiontype_id` INT NOT NULL,
  `course_id` INT NOT NULL,
  `creator_id` INT NOT NULL,
 )

 ARG-Layout: directory year month day numberOfFiles
 */
public class App {
    public static void main(String[] args) {
        if (args.length == 1 || args.length == 5) {
            try {


                LocalDate startDate = LocalDate.now();
                int numberOfDays = 1;

                if (args.length == 5) {
                    int year = Integer.parseInt(args[1]);
                    int month = Integer.parseInt(args[2]);
                    int day = Integer.parseInt(args[3]);
                    startDate = LocalDate.of(year,month,day);

                    numberOfDays = Integer.parseInt(args[4]);
                }

                for(int i = 0; i < numberOfDays; i++){
                    FileWriter fw = new FileWriter(args[0] + "/courses_" + (startDate.plusDays(i)).format(DateTimeFormatter.BASIC_ISO_DATE) + ".txt");
                    BufferedWriter bw = new BufferedWriter(fw);
                    ArrayList<String> rows = new ArrayList<String>();
                    Random r = new Random();
                    int numberOfValidRows = r.nextInt(5) + 20;
                    int numberOfInvalidRows = r.nextInt(1) + 2;

                    for(int j = 0; j<numberOfValidRows; j++) {
                        rows.add(RowGenerator.generateValidLine());
                    }
                    /*for(int k = 0; k<numberOfInvalidRows; k++) {
                        rows.add(RowGenerator.generateInvalidLine());
                    }*/

                    Collections.shuffle(rows);

                    bw.write("VALID ROWS: " + numberOfValidRows);
                    bw.newLine();
                    bw.write("INVALID ROWS: " + numberOfInvalidRows);
                    bw.newLine();
                    bw.write("TOTAL ROWS: " + (numberOfValidRows + numberOfInvalidRows));
                    bw.newLine();
                    bw.write("PlayerID\tPlayerName\tBirthdate\tPlayerScore");
                    bw.newLine();
                    for(String str : rows) {
                        bw.write(str);
                        bw.newLine();
                    }

                    bw.close();
                    fw.close();
                }




            } catch (IOException e){
                System.out.println("Incorrect file specified!");
            } catch (DateTimeException dte) {
                System.out.println("Incorrect date parameters specified!");
            }
        } else {
            System.out.println("Incorrect number of parameters specified!");
        }


    }
}
